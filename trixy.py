#!/usr/bin/python
import subprocess
import re
import os
import requests
import argparse
pwd=os.getcwd()
#parse command line arguement
parser = argparse.ArgumentParser()
parser.add_argument("--user", "-u", help="Username")
parser.add_argument("--password", "-p", help="Password")
parser.add_argument("--target", "-t", help="url for POST request")
args = parser.parse_args()


user=args.user
password=args.password
target=args.target

while True:
    # prevents lots of python error output
    try:
        s = raw_input('trixy>')
    except:
        break

    # check if you should exit
    if s.strip().lower() == 'exit':
        break

    # try to run command
    try:
        url = target
        myobj = {'user': user, 'password': password, 'command': s}
        x = requests.post(url, data = myobj)
        print(x.text)

    except OSError:
        print 'Invalid command'
