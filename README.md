Trixy webshell and command line interface. Runs as unprivileged user www-data on target machine.
Does not include privilege escalation at this time. This is a learning project for me as I'm not very good with PHP.
Load trixy.php to target machine in /var/www/, /var/www/html/, or whathaveyou. Run trixy.py from local machine.

optional arguments:
  -h, --help            show this help message and exit
  --user USER, -u USER  Username
  --password PASSWORD, -p PASSWORD
                        Password
  --target TARGET, -t TARGET
                        url for POST request

Example: python trixy.py -u root -p password -t http://192.168.0.52/trixy.php

Simple Linux shell with nothing special. type exit or ^c to exit.
